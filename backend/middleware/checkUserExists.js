const db = require("../model");
const User = db.users;

checkDuplicateName = (req, res, next) => {
  // Username
  User.findOne({
    where: {
      name: req.body.name
    }
  }).then(user => {
    if (user) {
      res.status(400).send({
        message: "Failed! Username is already in use!"
      });
      return;
    }
    next();
  });
};

module.exports = checkDuplicateName;