const db = require("../model");
const User = db.users;
const Op = db.Sequelize.Op;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.register = (req, res) => {
  if(!req.body.name){
      res.status(400).send({
          message:"Name Cannot be empty"
      });
      return;
  }

  //Create User
  User.create({
    name: req.body.name,
    password: bcrypt.hashSync(req.body.password, 8),
    description: req.body.description
  })
    .then(user => {
        console.log('user',user);
        res.send({ message: "User registered successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
      });
    
};

exports.signin = (req, res) => {
    User.findOne({
      where: {
        name: req.body.name
      }
    })
      .then(user => {
        if (!user) {
          return res.status(404).send({ message: "User Not found." });
        }
  
        var passwordIsValid = bcrypt.compareSync(
          req.body.password,
          user.password
        );
  
        if (!passwordIsValid) {
          return res.status(401).send({
            accessToken: null,
            message: "Invalid Password!"
          });
        }
  
        var token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
          expiresIn: 86400 // 24 hours
        });


          res.status(200).send({
            id: user.id,
            username: user.username,
            email: user.email,
            accessToken: token
          });
      })
      .catch(err => {
        res.status(500).send({ message: err.message });
      });
  };

  exports.userProfile = (req, res) => {
    User.findOne({
        where:{
            id:req.id
        }
    }).then(user => {
        if(!user){
            return res.status(404).send({message : "User not found"});
        }

        res.status(200).send({
            user:user
        });
    });
  };
