const db = require("../model");
const Products = db.products;
const Op = db.Sequelize.Op;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.getItems = (req, res) => {
    Products.findAll({
        where:{
            user_id: req.id
        }
    }).then(products => {
        res.status(200).json(products);
    }).catch(err => {
        res.status(500).send({ message: err.message });
      });
};

exports.getItem = (req, res) => {
    Products.findByPk(id).then(product => {
        res.status(200).json(product);
    }).catch(err => {
        res.status(500).send({message: err.message});
    }) 
}

exports.addItem = (req, res) => {
    product = {
        user_id : req.id,
        price: req.body.price,
        category: req.body.category,
        title: req.body.title,
        description:  req.body.description
    };
    Products.create(product).then(product => {
        res.status(200).send({message:'Product Added successfully',success:true});
    }).catch(err => {
        res.status(500).send({ message: err.message });
      });
}

exports.deleteItem = (req, res) => {
    Products.destroy({where:{id:req.params.id}});
        res.status(200).send({
            success:true,
            message:"deleted"
    });
}
