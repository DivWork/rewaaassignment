const express = require('express');
const router = express.Router();
const checkUserExists = require('../middleware/checkUserExists');

const ctrlUser = require('../controllers/user.controller');
const ctrlProducts = require('../controllers/products.controller');
const jwtHelper = require('../config/jwtHelper');

router.post('/register', checkUserExists , ctrlUser.register);
router.post('/signin', ctrlUser.signin);
// router.post('/updateUserInfo', jwtHelper.verifyJwtToken, ctrlUser.updateUserInfo);
router.get('/userProfile', jwtHelper.verifyJwtToken, ctrlUser.userProfile);

router.get('/getItems', jwtHelper.verifyJwtToken,ctrlProducts.getItems);
router.post('/addItem', jwtHelper.verifyJwtToken,ctrlProducts.addItem);
router.delete('/deleteItem/:id', jwtHelper.verifyJwtToken, ctrlProducts.deleteItem);

module.exports = router;