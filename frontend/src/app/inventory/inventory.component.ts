import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  form: any = {};
  errorMessage = '';
  items = [];

  constructor(private authService: AuthService,) { }

  ngOnInit(): void {
    this.authService.getItems().subscribe(
      data => {
        this.items = data;
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  onSubmit() {
    console.log(this.form);
    this.authService.addItem(this.form).subscribe(
      data => {
        console.log(data);
        if(data.success){
          alert('item added successfully');
          this.reload();
        }
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  deleteItem(id){
    this.authService.deleteItem(id).subscribe(
      data => {
        if(data.success){
          alert('delete successfully');
          this.reload();
        }
      },
      err => {
        this.errorMessage = err.error.message;
      }
    )
  }

  reload(){
    window.location.reload()
  };

}
