import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';

import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;
  userId: any;

  constructor(private token: TokenStorageService, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.profile().subscribe(
      data => {
        this.currentUser = data.user;
        console.log(this.currentUser);
      },
      err => {
        console.log(err);
      }
    );
  }

}
