import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:8080/api/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      name: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'register', {
      name: user.username,
      description: user.description,
      password: user.password
    }, httpOptions);
  }


  profile(): Observable<any>{
    return this.http.get(AUTH_API + 'userProfile', httpOptions);
  }

  getItems(): Observable <any> {
    return this.http.get(AUTH_API+'getItems', httpOptions);
  }

  addItem(item): Observable <any> {
    return this.http.post(AUTH_API+'addItem',{
      title: item.title,
      price: item.price,
      description: item.description,
      category: item.category
    },httpOptions);
  }

  deleteItem(id): Observable <any> {
    return this.http.delete(AUTH_API+'deleteItem/'+id,httpOptions);
  }
}
